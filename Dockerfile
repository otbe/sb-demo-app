FROM maven:3.6.1-amazoncorretto-8 AS build

ENV APP_HOME=/root/dev/myapp/
WORKDIR $APP_HOME

COPY pom.xml $APP_HOME/pom.xml

RUN mvn dependency:go-offline

COPY src $APP_HOME/src

RUN mvn package

FROM amazoncorretto:8u212

WORKDIR /root/

COPY --from=build /root/dev/myapp/target/demo-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java","-jar","demo-0.0.1-SNAPSHOT.jar"]