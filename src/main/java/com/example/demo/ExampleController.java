package com.example.demo;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class ExampleController {
    private Counter m = Metrics.counter("get user");

    @GetMapping("/")
    public List<UserDto> getUsers() {
        val list = new ArrayList<UserDto>();

        log.info("Foo");

        log.debug("bar");

        list.add(new UserDto("1"));

        m.increment();

        return list;
    }
}
